# mc01.py
#
# Create test for memtal calculation for my 8y old son.
# Execution: python3 mc.py
# Output: Screen and file (mcYYYYMMDD-HHMMSS.txt)
# Screen output example:
    # 5 + 4=		4 - 3=		7 - 4=		0 - 6=		
    # 3 - 6=		3 + 4=		9 + 10=		7 + 6=		
    # 5 + 7=		2 + 3=		7 - 1=		7 - 4=		
    # 7 - 3=		1 - 5=		10 - 5=		7 + 6=		
    # 8 - 8=		1 + 5=		10 - 7=		9 - 8=		
    # 8 + 8=		3 + 8=		3 - 9=		4 + 9=		
    # 2 - 0=		2 - 2=		4 - 5=		2 + 9=		
    # 5 + 6=		5 + 4=		1 - 1=		7 - 7=		
    # 10 - 8=		5 - 9=		4 + 2=		0 - 9=		
    # 3 - 8=		4 - 1=		6 - 2=		2 - 2=

from random import randrange
from datetime import datetime

OPERATIONS = ["+", "-"]
COLS = 4
ROWS = 10

def create_operations(cols, rows):
    ops = []
    for op in range(cols * rows):
        operation = []
        operation.append(randrange(11))
        numeral2 = randrange(11)
        math_operation = randrange(2)
        if math_operation == 0:
            math_operation_result = operation[0] + numeral2
            operation.append('+')
        else:
            math_operation_result = operation[0] - numeral2
            operation.append('-')
        operation.append(numeral2)
        operation.append(math_operation_result)
        operation.append(math_operation)
        ops.append(operation)
    # print(f"{ops}")
    return ops
            

def display_operations(ops):
    for op in range(COLS * ROWS):
        line = str(ops[op][0]) + " " + str(ops[op][1]) + \
                " " + str(ops[op][2])
        print(f"{line}=\t\t", end = '')
        # print(f"{line_with_result=}")
        if not ((op + 1) % COLS):  # op+1 we avoid 0 % COLS -> True for 1st lin
            print("")


def write_ops_to_file(ops):
    # I know do two round is not the optimal way but it's more clear and it
    #   never will grow to much
    filename = "mc" + datetime.now().strftime("%Y%m%d-%H%M%S") + ".txt"
    with open(filename, 'w') as output_file:
        line_with_result = ""
        for op in range(COLS * ROWS):
            line_with_result = line_with_result + str(ops[op][0]) + " " + \
                str(ops[op][1]) + " " + str(ops[op][2]) + " = " + str(ops[op][3]) + str("\t\t")
            if not ((op + 1) % COLS):  # op+1 we avoid 0 % COLS -> True for 1st lin
                output_file.write(line_with_result + "\n")
                line_with_result = ""

def main():
    ops = create_operations(COLS, ROWS)
    display_operations(ops)
    write_ops_to_file(ops)


if __name__ == "__main__":
    main()
