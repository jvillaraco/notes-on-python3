#!/bin/python3
# -*- coding: utf-8 -*-

# deleteFromATmpDirectoryByDate.py
#
# Objective: The objetive for this script is maintain clean a Download
#  diretory/folder deleting files older than a param and directories
#  with creation date with same condition than files and void. It's true it
#  can be done with a cli command like find and -exec but void directory
#  management its not the same.
#
# Requirements:
# 1. Delete all files with creation date and execution date > d days(by params)
# 2. Delete all directories with creation date > d days (by param) and no
#     files in
#
# Execution:
#  python3 deleteFromATmpDirecoryByDate.py ~/esborrar/ -d 110

import argparse
from os import getcwd, path, listdir, remove, rmdir
from datetime import datetime, timedelta
from pathlib import Path


def remove_dirs(dirs_to_remove, dry_run):
    # print(f"{dry_run=} {type(dry_run)=}")
    for dir_to_remove in dirs_to_remove:
        print(f"Dir to remove: {dir_to_remove} ", end='')
        if dry_run:
            print(f" ... Deleted (really not (dry-run))")
        else:
            rmdir(dir_to_remove)
            print(f" ... Deleted")
    return True


def remove_files(files_to_remove, dry_run):
    # print(f"{dry_run=} {type(dry_run)=}")
    for file_to_remove in files_to_remove:
        print(f"File to remove: {file_to_remove} ", end='')
        if dry_run:
            print(f" ... Deleted (really not (dry-run))")
        else:
            remove(file_to_remove)
            print(f" ... Deleted")
    return True


def get_dirs_to_remove(dirs, days):
    dirs_to_remove = []
    for dir in dirs:
        dir_mtime = datetime.fromtimestamp(path.getmtime(dir))
        # print(f"{dir}: {dir_mtime=} "
        #       f"{datetime.now() - timedelta(days=days)}")
        dir_empty = not listdir(dir)
        if (dir_mtime < (datetime.now() - timedelta(days=days))) and \
                dir_empty:
            dirs_to_remove.append(dir)
    return dirs_to_remove


def get_files_to_remove(files, days):
    files_to_remove = []
    for file in files:
        file_mtime = datetime.fromtimestamp(path.getmtime(file))
        # print(f"{file}: {file_mtime}="
        #       f" {datetime.datetime.now() - datetime.timedelta(days=days)}=")
        if file_mtime < (datetime.now() - timedelta(days=days)):
            files_to_remove.append(file)
    return files_to_remove


def get_files_and_dirs(path):
    '''get a pathlib.Path directory object and return files and folders in 2
    diff lists.
    '''
    # print(list(path.iterdir()))
    # print(f"{list(path.rglob('*'))}=")
    files = []
    dirs = []
    for item in list(path.rglob('*')):
        if item.is_file():
            files.append(item)
        else:
            dirs.append(item)
    return files, dirs


def get_params():
    '''Get all params from commandline and return them in an Namespace object.
    '''
    parser = argparse.ArgumentParser(prog='deleteFromATmpDirectoryByDate.py',
                                     description='Delete files and direcories'
                                                 ' depending on date')
    # Optional arguments
    parser.add_argument('directory', help='The tarjet directory', nargs='?',
                        type=Path)
    parser.add_argument('-d', '--days', default='7',
                        help='Maximun days to keep in directory (default 7)')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Enable verbose output')
    parser.add_argument('-n', '--num-records', type=int, default=10,
                        help='The number of records to process')
    parser.add_argument('--dry-run', action='store_true',
                        default=False,
                        help='Execute script but not do any changes'
                             ' in filesystem')
    args = parser.parse_args()
    return args


def main():
    args = get_params()
    print(f"{args=}\n")
    files, dirs = get_files_and_dirs(vars(args).get('directory'))
    # print(f"{files=} {dirs=}")
    files_to_remove = get_files_to_remove(files, int(vars(args).get('days')))
    # print(f"\nFiles to remove: {files_to_remove}")
    dirs_to_remove = get_dirs_to_remove(dirs, int(vars(args).get('days')))
    # print(f"\nDirs to remove: {dirs_to_remove}")
    remove_files(files_to_remove, vars(args).get('dry_run'))
    remove_dirs(dirs_to_remove, vars(args).get('dry_run'))


if __name__ == '__main__':
    main()
