#!/bin/python3
# -*- coding: utf-8 -*-

# Author: Joan G. Villaraco (if you ever use this code mention me)
# https://www.hackerrank.com/challenges/finding-the-percentage/
#
# The provided code stub will read in a dictionary containing key/value pairs
#  of name:[marks] for a list of students. Print the average of the marks
#  array for the student name provided, showing 2 places after the decimal.
#
# Execute: python3 finding_the_percentage.py < finding_the_percentage_in.txt | tee finding_the_percentage_out.txt

b_DEBUG = True

def student_average(marks):
    return f"{sum(marks) / len(marks):.2f}"


def get_student_marks_average(student_marks, query_name):
    return student_average(student_marks[query_name])

if __name__ == '__main__':
    while True:
        try:
            n = input()
        except EOFError:
            break

        student_marks = {}
        for _ in range(int(n)):
            name, *line = input().split()
            scores = list(map(float, line))
            student_marks[name] = scores
        query_name = input()

        if b_DEBUG: print(f"{student_marks=} {query_name=} student_marks[{query_name}]:{student_marks[query_name]} ")

        result = get_student_marks_average(student_marks, query_name)
        print(result)
