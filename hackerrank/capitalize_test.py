#!/bin/python3
# -*- coding: utf-8 -*-

# capitalize_test.py
#
# Execute: pytest -v -s capitalize_test.py 
# Result:
#   pytest -v capitalize_test.py 
    # ==================================================== test session starts ====================================================
    # platform linux -- Python 3.9.2, pytest-7.1.2, pluggy-0.13.0 -- /usr/bin/python3
    # cachedir: .pytest_cache
    # rootdir: /home/villaraco/treball/dev/python/notes-on-python3/hackerrank
    # collected 18 items                                                                                                          

    # capitalize_test.py::test_base[alison heck-Alison Heck] PASSED                                                         [  5%]
    # capitalize_test.py::test_base[joan g. villaraco-Joan G. Villaraco] PASSED                                             [ 11%]
    # capitalize_test.py::test_base[1234 5678-1234 5678] PASSED                                                             [ 16%]
    # capitalize_test.py::test_base[a       a-A       A] PASSED                                                             [ 22%]
    # capitalize_test.py::test_base[ a a- A A] PASSED                                                                       [ 27%]
    # capitalize_test.py::test_base[a-A] PASSED                                                                             [ 33%]
    # capitalize_test.py::test_base[1-1] PASSED                                                                             [ 38%]
    # capitalize_test.py::test_base[ - ] PASSED                                                                             [ 44%]
    # capitalize_test.py::test_base[-] PASSED                                                                               [ 50%]
    # capitalize_test.py::test_base2[alison heck-Alison Heck] PASSED                                                        [ 55%]
    # capitalize_test.py::test_base2[joan g. villaraco-Joan G. Villaraco] PASSED                                            [ 61%]
    # capitalize_test.py::test_base2[1234 5678-1234 5678] PASSED                                                            [ 66%]
    # capitalize_test.py::test_base2[a       a-A       A] PASSED                                                            [ 72%]
    # capitalize_test.py::test_base2[ a a- A A] PASSED                                                                      [ 77%]
    # capitalize_test.py::test_base2[a-A] PASSED                                                                            [ 83%]
    # capitalize_test.py::test_base2[1-1] PASSED                                                                            [ 88%]
    # capitalize_test.py::test_base2[ - ] PASSED                                                                            [ 94%]
    # capitalize_test.py::test_base2[-] PASSED                                                                              [100%]
    # ==================================================== 18 passed in 0.03s =====================================================


import pytest
from datetime import datetime, timedelta
from capitalize import solve, solve2

data_test_base = [
                  ["alison heck", "Alison Heck"],
                  ["joan g. villaraco", "Joan G. Villaraco"],
                  ["1234 5678", "1234 5678"],
                  ["a       a", "A       A"],
                  [" a a", " A A"],
                  ["a", "A"],
                  ["1", "1"],
                  [" ", " "],
                  ["", ""],
                 ]


@pytest.mark.parametrize("s_in,s_out", data_test_base)
def test_base(s_in, s_out):
    print(f"{s_in=} {s_out=}")
    assert solve(s_in) == s_out


@pytest.mark.parametrize("s_in,s_out", data_test_base)
def test_base2(s_in, s_out):
    print(f"{s_in=} {s_out=}")
    assert solve2(s_in) == s_out