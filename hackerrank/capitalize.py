#!/bin/python3
# -*- coding: utf-8 -*-

# Author: Joan G. Villaraco (if you ever use this code mention me)
# https://www.hackerrank.com/challenges/capitalize
#
# You are asked to ensure that the first and last names of people begin with a capital letter in their passports. For example, alison heck should be capitalised correctly as Alison Heck.
# Given a full name, your task is to capitalize the name appropriately.
#
# Execute: python3 capitalize.py < capitalize_in.txt | tee capitalize_out.txt
# print(s.title()) # Programming Is Awesome

b_DEBUG = True

def solve2(s):
    # Must be " " in order to solve the case where letter is the first one in s
    previous_letter = " "
    s_words_capitalized = []
    for letter in s:
        if previous_letter == " " and letter.islower():
            s_words_capitalized.append(letter.capitalize())
        else:
            s_words_capitalized.append(letter)
        previous_letter = letter
    return "".join(s_words_capitalized)

def solve(s):
    return s.title()

if __name__ == '__main__':
    while True:
        try:
            s = input()
        except EOFError:
            break
        if b_DEBUG: print(f"s: {s}")
        result = solve(s)

        print(result)