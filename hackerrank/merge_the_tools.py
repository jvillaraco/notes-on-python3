#!/bin/python3
# -*- coding: utf-8 -*-

# Author: Joan G. Villaraco (if you ever use this code, mention me)
# https://www.hackerrank.com/challenges/merge-the-tools/
#
#
# Execute: python3 merge_the_tools.py < merge_the_tools_in.txt | tee\
#   merge_the_tools_out.txt
#
from collections import OrderedDict

# b_DEBUG = True
b_DEBUG = False


def filter_substrings(substrings):
    substrings_not_repeated = []
    for substring in substrings:
        substring_not_repeated = list(OrderedDict.fromkeys(substring))
        # if b_DEBUG: print(f"{substring_not_repeated=}")
        substrings_not_repeated.append(''.join(substring_not_repeated))
    return substrings_not_repeated


def merge_the_tools(string, k):
    substrings = [string[i:i+k] for i in range(0, len(string), k)]
    if b_DEBUG: print(f"{substrings=}")
    for individual_res in filter_substrings(substrings):
        print(f"{individual_res}")


if __name__ == '__main__':
    while True:
        try:
            string, k = input(), int(input())
        except EOFError:
            break
        if b_DEBUG: print(f"s: {string=} {k=}")
        result = merge_the_tools(string, k)
