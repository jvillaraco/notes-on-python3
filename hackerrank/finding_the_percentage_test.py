#!/bin/python3
# -*- coding: utf-8 -*-

# finding_the_percentage_test.py
#
# Execute: pytest -v finding_the_percentage_test.py 
# Result:
#   pytest -v finding_the_percentage_test.py
    # ==== test session starts =====
    # platform linux -- Python 3.9.2, pytest-7.1.2, pluggy-0.13.0 -- /usr/bin/python3
    # cachedir: .pytest_cache
    # rootdir: /home/villaraco/treball/dev/python/notes-on-python3/hackerrank
    # collected 8 items                                                                                                                                         

    # finding_the_percentage_test.py::test_student_average[list0-56.00] PASSED                              [ 12%]
    # finding_the_percentage_test.py::test_student_average[list1-1.00] PASSED                               [ 25%]
    # finding_the_percentage_test.py::test_student_average[list2-100.00] PASSED                             [ 37%]
    # finding_the_percentage_test.py::test_student_average[list3-0.00] PASSED                               [ 50%]
    # finding_the_percentage_test.py::test_student_average[list4-68.00] PASSED                              [ 62%]
    # finding_the_percentage_test.py::test_get_student_marks_average[3-marks0-K-68.00] PASSED               [ 75%]
    # finding_the_percentage_test.py::test_get_student_marks_average[2-marks1-A-0.00] PASSED                [ 87%]
    # finding_the_percentage_test.py::test_get_student_marks_average[3-marks2-K-100.00] PASSED              [100%]

    # ===== 8 passed in 0.02s =====


import pytest
from finding_the_percentage import student_average, get_student_marks_average

# Atention to the result, it has to be a a number (or string) with 2 decimals
marks_test_base = [
                  [[52, 56, 60], "56.00"],
                  [[1, 1, 1], "1.00"],
                  [[100, 100, 100], "100.00"],
                  [[0, 0, 0], "0.00"],
                  [[67, 68, 69], "68.00"],
                 ]

@pytest.mark.parametrize("list,result", marks_test_base)
def test_student_average(list,result):
    print(f"{list=} {result=}")
    assert str(student_average(list)) == result

data_test_base = [
                  [3,["K 67 68 69", "A 70 98 63", "M 52 56 60"], "K", "68.00"],
                  [2,["K 100 100 100", "A 0 0 0"], "A", "0.00"],
                  [3,["J 0 0 0", "K 100 100 100", "A 0 0 0"], "K", "100.00"],
                 ]

@pytest.mark.parametrize("n,marks,name,result", data_test_base)
def test_get_student_marks_average(n, marks, name, result):
    print(f"{n=} {marks=} {name=} {result=}")
    # finding_the_percentage.py code variables names are preserved for do 
    #   recreate get_student_marks_average call with same parameters
    student_marks = {}
    for i in range(int(n)):
        print(f"{i=}")
        print(f"{marks[i].split()=}")
        name_tmp, *line = marks[i].split()
        scores = list(map(float, line))
        student_marks[name_tmp] = scores
    query_name = name
    print(f"{student_marks=} {query_name=} {student_marks[f'{query_name}']=}")
    print(f"get_... {get_student_marks_average(student_marks, query_name)}")
    assert get_student_marks_average(student_marks, query_name) == result
