# Datetime manipulation in python3

python datetime_rutines.py 

    datetime.datetime.utcnow(): 2020-06-08 15:24:56.720863
    datetime.datetime.utcnow()+ datetime.timedelta(seconds=100): 2020-06-08 15:26:36.721133
    diff: 0:01:40.000270
    diff seconds: 100.00027
    divmod[0] Minutes: 1.0
    divmod[1] remainder seconds: 40.00027
    iids s_dt_date[:-6]: 2020-03-10T16:44:38.142626
    Date: 2020-03-10
    Time: 16:44:38.142626
    iids s_dt_date[:-6]: 2020-03-10T16:44:38.142626
    Date: 2020-03-10
    Time: 15:44:38.142626
    iids s_dt_date[:-6]: 2020-03-10T16:44:38.142626
    Date: 2020-03-10
    Time: 16:44:38.142626
    Datetime in sortable number: 20200310164438142626