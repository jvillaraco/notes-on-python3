# Testing datetime methods

import datetime


def import_isoformat_dt_string(s_dt_date, b_utc=True):
    '''Rutine to import iso format and generate a datetime object to handle it
    IN: <s_dt_date: string> iso format datetime string
        <b_utc: bool> is s_dt_date using utcoffset char?
    OUT: <d: datetime> s_dt_date translated to datetime object'''
    # Derived from https://gist.github.com/kkAyataka/ (Thankx KKAvataka)
    if s_dt_date[-1:] == 'Z':
        d = datetime.datetime.strptime(s_dt_date[:-1], '%Y-%m-%dT%H:%M:%S')
    elif s_dt_date[-6:-5] == '+' or s_dt_date[-6:-5] == '-':
        sign = s_dt_date[-6:-5]
        hours = int(s_dt_date[-5:].split(':')[0])
        minutes = int(s_dt_date[-5:].split(':')[1])
        offsetmin = hours * 60 + minutes
        if sign == '-':
            offsetmin *= -1
        utcoffset = datetime.timedelta(minutes=offsetmin)
        print("iids s_dt_date[:-6]: %s" % s_dt_date[:-6])
        d = datetime.datetime.strptime(s_dt_date[:-6],
                                       '%Y-%m-%dT%H:%M:%S.%f')
        if b_utc:
            d -= utcoffset
    return d


dt_d1 = datetime.datetime.utcnow()
print("datetime.datetime.utcnow(): %s" % dt_d1)
dt_d2 = datetime.datetime.utcnow() + datetime.timedelta(seconds=100)
print("datetime.datetime.utcnow()+ datetime.timedelta(seconds=100): %s" %
      dt_d2)
print('datetime.datetime.utcnow()strftime("%%Y-%%m-%%d %%H:%%M:%%S"): %s' %
      datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))

dt_diff = dt_d2 - dt_d1
print("diff: %s" % dt_diff)

dt_sec = dt_diff.total_seconds()
print("diff seconds: %s" % dt_sec)

dt_min = divmod(dt_sec, 60)
print("divmod[0] Minutes: %s" % dt_min[0])
print("divmod[1] remainder seconds: %s" % dt_min[1])


# Import iso format from string (UTC)
s_date = "2020-03-10T16:44:38.142626+00:00"
dt_date = import_isoformat_dt_string(s_date)
print("Date: %s" % dt_date.date())
print("Time: %s" % dt_date.time())

# Import iso format from string (UTC+1) output UTC
s_date = "2020-03-10T16:44:38.142626+01:00"
dt_date = import_isoformat_dt_string(s_date)  # Using default param.
print("Date: %s" % dt_date.date())
print("Time: %s" % dt_date.time())

# Import iso format from string (UTC+1) output UTC
s_date = "2020-03-10T16:44:38.142626+01:00"
dt_date = import_isoformat_dt_string(s_date, b_utc=False)
print("Date: %s" % dt_date.date())
print("Time: %s" % dt_date.time())

# Print imported date to sortable number
print("Datetime in sortable number: %s" %
      dt_date.strftime('%Y%m%d%H%M%S%f'))
