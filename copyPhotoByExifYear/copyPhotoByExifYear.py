#!/bin/python3
# -*- coding: utf-8 -*-

# copyPhotoByExifYear.py
#
# Objective: Classify photos from exif info to their creation year. An easy
#   tool for myself.
#
# Requirements:
# * Debian package python3-exifread: apt install ...
#
# Execution:
#  <from dir with photos>$ python3 ~/treball/dev/python/notes-on-python3/copyPhotoByExifYear/copyPhotoByExifYear.py

# import argparse
from os import makedirs, path, rename, sep
# from datetime import datetime, timedelta
from pathlib import Path
import exifread
from PIL import Image


def get_files_and_dirs(path):
    '''get a pathlib.Path directory object and return files and folders in 2
    diff lists.
    '''
    # print(list(path.iterdir()))
    # print(f"{list(path.rglob('*'))}=")
    files = []
    dirs = []
    for item in list(Path(path).rglob('*')):
        if item.is_file():
            files.append(item)
        else:
            dirs.append(item)
    return files, dirs


def delete_no_images(files):
    '''Delete from files all file not an image'''
    images = []
    for file in files:
        try:
            im = Image.open(file)
        except Exception as err:
            print(f"Error with {file}: ignoring file.")
        else:
            images.append(file)
    return images


def get_year_for_files(files):
    '''Create a dict with filename and year with exif info'''
    files_with_year = []
    for file in files:
        im = Image.open(file)
        tags = {}
        with open(file, 'rb') as f:
            tags = exifread.process_file(f, details=False)
        # print(f"{tags.get('Image DateTime')=}")
        # print(f"{str(tags.get('Image DateTime'))[:4]=}")
        file_and_year = {'file': file,
                         'year': str(tags.get('Image DateTime'))[:4]}
        files_with_year.append(file_and_year)
    return files_with_year


def create_dir_if_needed(year):
    '''Create directory just in case in doesn't exists'''
    print(f"{year=} {type(year)}")
    if not path.exists(year):
        makedirs(year)


def move_file_to_year_dir(files_with_year):
    '''Move every file to it's year directory'''
    for file_with_year in files_with_year:
        if not file_with_year.get('year') == "None":
            create_dir_if_needed(file_with_year.get('year'))
            final_dir = file_with_year.get('year') + str(sep) +\
                str(file_with_year.get('file'))
            # print(f"rename {file_with_year.get('file')} {final_dir}")
            rename(file_with_year.get('file'), final_dir)
    return True


def main():
    files, dirs = get_files_and_dirs('.')
    # print(f"{files=} {dirs=}")
    images = delete_no_images(files)
    files_with_year = get_year_for_files(images)
    print(f"{files_with_year=}")
    move_file_to_year_dir(files_with_year)


if __name__ == '__main__':
    main()
