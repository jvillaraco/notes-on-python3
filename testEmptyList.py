def listEmpty(l):
	if not l:
		print("Empty list")
	else:
		print("Not empty")
	return(not l)

l = []
print(f"list {l} is empty? {listEmpty(l)}")
l = [1]
print(f"list {l} is empty? {listEmpty(l)}")
	
