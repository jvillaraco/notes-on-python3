# addPriceFilename.py
#
# Files to add has this format <filename_NNNN>.<ext>. I.e.: foo_1514.bar counts as 15.14€

import pathlib
from os import listdir
from os.path import isfile, join, splitext
from sys import argv


def total_price(dir):

    onlyfiles = [f for f in listdir(dir) if isfile(join(dir, f))]

    print(f"Files is this dir: {onlyfiles}")

    costs = []
    total_euros = 0

    for file in onlyfiles:
        filename, file_extension = splitext(file)
        cost = filename[-4:]
        costs.append(cost)

        euros = int(cost)/100
        print(f"{euros=}")

        total_euros += euros

        print(f"{filename=} ==> {euros} ==> {total_euros:.2f}")

    print(f"{costs=}")

    return total_euros


if __name__ == "__main__":
    print(f"Total: {total_price(argv[1]):.2f}€")