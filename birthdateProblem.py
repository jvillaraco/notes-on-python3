# -*- coding: utf-8 -*-
#
# Test for birthday problem
# https://en.wikipedia.org/wiki/Birthday_problem
#
# $ wcalc
# -> 1-(365!/(365^23*(365-23)!))
# = 0.507297
#
# Execute: python3 birthdateProblem.py
# Example results:
    # NUMBER_OF_EXECUTIONS=10
    # PEOPLE_NUMBER=23
    # Quantity of repeated birth_dates = 1
    # Quantity of repeated birth_dates = 0
    # Quantity of repeated birth_dates = 0
    # Quantity of repeated birth_dates = 1
    # Quantity of repeated birth_dates = 1
    # Quantity of repeated birth_dates = 1
    # Quantity of repeated birth_dates = 2
    # Quantity of repeated birth_dates = 0
    # Quantity of repeated birth_dates = 1
    # Quantity of repeated birth_dates = 0

from random import randint

# Constants
PEOPLE_NUMBER = 23
NUMBER_OF_EXECUTIONS = 10


def create_birth_dates(birth_dates):
    birth_dates = [randint(1,365) for i in range(PEOPLE_NUMBER)]
    # print(birth_dates)
    return birth_dates


def test_birth_dates(birth_dates):
    seen = set()
    repeated_birth_dates = [x for x in birth_dates if x in seen or seen.add(x)]
    # print(f"{repeated_birth_dates=}")
    # print(f"{seen=}")
    quantity_repeated_birth_dates = len(repeated_birth_dates)
    return quantity_repeated_birth_dates


def main():
    ''' main routine '''
    print(f"{NUMBER_OF_EXECUTIONS=}")
    print(f"{PEOPLE_NUMBER=}")
    for execution in range(NUMBER_OF_EXECUTIONS):
        birth_dates = []
        birth_dates = create_birth_dates(birth_dates)
        quantity_repeated_birth_dates = test_birth_dates(birth_dates)
        print(f"Quantity of repeated birth_dates = {quantity_repeated_birth_dates}")


if __name__ == '__main__':
    main()